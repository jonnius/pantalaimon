# Pantalaimon for Ubuntu Touch
[Pantalaimon](https://github.com/matrix-org/pantalaimon) can be used to enable end-to-end-encryption with Fluffy Chat. The Instructions in this repository are
work in progress. Feel free to contribute.

## Instructions
### Setup
Transfer the build script (setup-pantalaimon.sh) to your Ubuntu Touch device,
e.g. via SSH:

```sh
scp setup-pantalaimon.sh phablet@<PHONE-IP>:/home/phablet
```

Enter a Shell (e.g. SSH) Run the scipt and apend your Home Server:

```sh
./setup-pantalaimon.sh https://matrix.org/
```

Pantalaimon should start automatically following successfull installation.

### Usage

To start Pantalaimon (using Terminal or ssh):

```sh
start pantalaimon
```

To stop Pantalaimon (using Terminal or ssh):

```sh
stop pantalaimon
```

In Fluffy Chat use the *Pantalaimon* button where you choose your Home Server
and log in with your account.

### Configuration
If you want to configure Pantalaimon, e.g. change the Home Server, refer to
the [Pantalaimon documentation](https://github.com/matrix-org/pantalaimon/blob/master/docs/man/pantalaimon.8.md). You'll find the config file at `/home/phablet/pantalaimon/pantalaimon.conf`.

## What next?

There are basically two ways to proceed. Either put Pantalaimon in its own app.
Or ship Pantalaimon with FluffyChat. The first should be easier to achieve while
the latter could provide a smoother user experience. Feel free to contribute!

### Pantalaimon App
What an app should be able to do:
- Allow to configure the Home Server (write it to the config file)
- Install an upstart service to start Pantalaimon automatically in the background
- Allow to start, stop, enable and disable the upstart service

**Note**: Such an app needs to be unconfined, in order to install an upstart
service and to open a network socket.

### FluffyChat Integration
Steps to be taken:
- Ship Pantalaimon with FluffyChat
- Make FluffyChat start Pantalaimon on app start if the user chose Pantalaimon as Home Server
- Patch Pantalaimon to [enable Unix Domain Sockets](https://github.com/matrix-org/pantalaimon/issues/46) (this allows FluffyChat to remain confined)
- Enable FluffyChat to connect to a Unix Domain Socket instead of a Network Socket
