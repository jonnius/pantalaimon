#!/bin/bash
set -Eeuo pipefail

PROJECT_DIR=~/pantalaimon
TMP_DIR="${PROJECT_DIR}/tmp"

VIRTENV_DIR="${PROJECT_DIR}/virtenv"
VIRTENV_ACTIVATE="${VIRTENV_DIR}/bin/activate"

OLM_DIR="${PROJECT_DIR}/olm"
PANTALAIMON_DIR="${PROJECT_DIR}/pantalaimon"
PANTALAIMON_START_SCRIPT="${PROJECT_DIR}/pantalaimon.sh"
PANTALAIMON_START_SCRIPT_BACKGROUND="${PROJECT_DIR}/pantalaimon-bg.sh"
PANTALAIMON_CONF_FILE="${PROJECT_DIR}/pantalaimon.conf"
PANTALAIMON_UPSTART_SCRIPT=~/.config/upstart/pantalaimon.conf

PYTHON_VERSION_MINOR="3.7"
PYTHON_VERSION_PATCH="7"
PYTHON_VERSION="${PYTHON_VERSION_MINOR}.${PYTHON_VERSION_PATCH}"
PYTHON_DIR="${PROJECT_DIR}/python"
PYTHON_SRC_DIR="${TMP_DIR}/Python-${PYTHON_VERSION}"
PYTHON="${PYTHON_DIR}/bin/python${PYTHON_VERSION_MINOR}"

if [ $# != 1 ]; then
	echo "This script installs Pantalaimon on Ubuntu Touch. It is meant to be executed on the device directly."
	echo "Please provide the Matrix Server URL. You can change it later by editing ${PANTALAIMON_CONF_FILE}."
	echo -e "\nUsage:\n $0 [Matrix-Server]"
	echo -e "\nExample:\n $0 https://ubports.chat/"
	exit 1
fi

HOME_SERVER=$1

case $HOME_SERVER in
	https://*)
		echo "Configuring Home Server as $HOME_SERVER"
		echo "You can change it later inside ${PANTALAIMON_CONF_FILE}"
		;;
	*)
		echo "The server address must start with 'https://'."
		echo -e "\nExample:\n $0 https://ubports.chat/"
		exit 1
esac

mkdir -p "${PROJECT_DIR}"
mkdir -p "${TMP_DIR}"
mkdir -p "${PANTALAIMON_DIR}"

PACKAGES="make cmake build-essential zlibc python3-venv libffi-dev ppa-purge libssl-dev libsqlite3-dev"

PANTALAIMON_CONF_CONTENT="
[local-matrix]
Homeserver = ${HOME_SERVER}
ListenAddress = localhost
ListenPort = 8009
IgnoreVerification = True
"

START_SCRIPT_BACKGROUND_CONTENT="
cd ${VIRTENV_DIR}
source bin/activate
export LD_LIBRARY_PATH=${OLM_DIR}/usr/local/lib/
pantalaimon -c ${PANTALAIMON_CONF_FILE} > ${PROJECT_DIR}/pantalaimon.log 2>&1 & disown
"

START_SCRIPT_CONTENT="
cd ${VIRTENV_DIR}
source bin/activate
export LD_LIBRARY_PATH=${OLM_DIR}/usr/local/lib/
pantalaimon -c ${PANTALAIMON_CONF_FILE}
"

UPSTART_SCRIPT_CONTENT="
start on started unity8
pre-start script
    initctl set-env LD_LIBRARY_PATH=/home/phablet/pantalaimon/olm/usr/local/lib:\$LD_LIBRARY_PATH
    sleep 30
end script
respawn
exec /home/phablet/pantalaimon/virtenv/bin/pantalaimon -c /home/phablet/pantalaimon/pantalaimon.conf
"

echo "Remounting RootFS writable..."
sudo mount -o remount,rw /

echo "Installing dependencies..."
sudo apt update
sudo apt install -y ${PACKAGES}

if [ ! -f "${OLM_DIR}/usr/local/lib/libolm.so" ]; then
    cd "${TMP_DIR}"
    echo "Downloading Olm..."
    wget -qO- "https://gitlab.matrix.org/matrix-org/olm/-/archive/master/olm-master.tar.gz" | tar -xz

    echo "Building Olm..."
    cd olm-master
    cmake . -Bbuild
    cmake --build build

    echo "Installing Olm..."
    cd build
    make install DESTDIR="${OLM_DIR}"
else
    echo "Olm found. Skipping Olm Install..."
fi

if [ ! -f "${PYTHON}" ]; then
    echo "Downloading Python..."
    cd "${TMP_DIR}"
    wget -qO- "https://www.python.org/ftp/python/${PYTHON_VERSION}/Python-${PYTHON_VERSION}.tgz" | tar -xz

    echo "Installing Python..."
    cd "${PYTHON_SRC_DIR}"
    ./configure --prefix="${PYTHON_DIR}"
    make
    make install
else
    echo "Python found. Skipping Python Install..."
fi

if [ ! -f "${VIRTENV_ACTIVATE}" ]; then
    echo "Setting up virtual environment..."
    ${PYTHON} -m venv "${VIRTENV_DIR}"
else
    echo "Virtual environment found. Skipping setup..."
fi

echo "Installing Pantalaimon..."
cd "${VIRTENV_DIR}"
source "${VIRTENV_ACTIVATE}"

CFLAGS=-I"${OLM_DIR}/usr/local/include -I${OLM_DIR}/usr/local/include" LDFLAGS="-L${OLM_DIR}/usr/local/lib" python${PYTHON_VERSION_MINOR} -m pip install --upgrade pantalaimon

echo "Creating Pantalaimon config."
echo "${PANTALAIMON_CONF_CONTENT}" > "${PANTALAIMON_CONF_FILE}"
echo "Creating Pantalaimon start script."
echo "${START_SCRIPT_CONTENT}" > "${PANTALAIMON_START_SCRIPT}"
echo "${START_SCRIPT_BACKGROUND_CONTENT}" > "${PANTALAIMON_START_SCRIPT_BACKGROUND}"
echo "${UPSTART_SCRIPT_CONTENT}" > "${PANTALAIMON_UPSTART_SCRIPT}"
chmod +x "${PANTALAIMON_START_SCRIPT}"
chmod +x "${PANTALAIMON_START_SCRIPT_BACKGROUND}"

echo "Adding ${PROJECT_DIR} to PATH."
echo "export PATH=\"$PATH:${PROJECT_DIR}\"" >> ~/.bashrc
source ~/.bashrc

echo "Successfully installed Pantalaimon."

echo "Cleaning up, reverting changes to RootFS..."
sudo apt autoremove -y ${PACKAGES}
sudo apt clean

echo
echo "Starting Pantalaimon."
start pantalaimon
